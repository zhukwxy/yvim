#!/bin/bash

username=`whoami`

curDir=`pwd`
#下载自动补全
yum install bash-completion -y
#安装依赖
#{{{
if [[ "$username" == "root" ]] ; then
    #yum install ncurses-devel python3-devel python-devel nodejs libXt-devel -y
    yum install ncurses-devel python3-devel python-devel -y
    yum install gcc gcc-c++ cmake ctags cscope -y 
fi
#}}}

#安装配置
#{{{
cp ./rpm.vimrc ~/.vimrc
cp vim ~/.vim -rf
chmod 777 ~/.vim/cscope/*
#}}}

#安装YCM
#{{{
cd ~/.vim/plugged
cat YouCompleteMe.tar.gza* | tar -xvz
cd ~/.vim/plugged/YouCompleteMe
./install.py --clangd-completer
#}}}

#安装nodejs
#{{{
cd $curDir
if [[ "$username" == "root" ]] ; then
    tar -xzvf ./src/rpm/node-v8.17.0-linux-x64.tar.gz --strip-components 1 -C /usr
fi
#}}}

#安装更新stdc++
#{{{
if [[ "$username" == "root" ]] ; then
    cp ./src/rpm/libstdc++.so.6.0.26 /lib64
    tar -cvf /lib64/libstdc++.so.6.tar.gz /lib64/libstdc++.so.6
    rm -rf /lib64/libstdc++.so.6
    ln -s /lib64/libstdc++.so.6.0.26 /lib64/libstdc++.so.6
fi
#}}}

#安装vim
#{{{
if [[ "$username" == "root" ]] ; then
    yum remove vim -y
    tar -xzvf $curDir/src/vim-8.2.0695.tar.gz -C $curDir/src
    cd $curDir/src/vim-8.2.0695
    ./configure --with-features=huge --enable-rubyinterp=yes --enable-pythoninterp=yes --enable-python3interp=yes --enable-luainterp=yes --enable-multibyte --with-python-command=python2 --with-python3-command=python3 --enable-cscope --enable-gnome-check --enable-motif-check --prefix=/usr --enable-largefile
    make -j4
    make install
    rm -rf $curDir/src/vim-8.2.0695
fi
#}}}
