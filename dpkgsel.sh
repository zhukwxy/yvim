#!/bin/bash
set -e
id=$(cat /etc/os-release | grep ^ID= | cut -d '=' -f 2)

function arch() {
	echo $(uname -m)
}
ARCH=$(arch)
exit 0
if [ "${id}x" == "uosx" ]; then
    . ./uosinst.sh
elif [ "${id}x" == "debianx" ] ; then 
    . ./debinst.sh
fi


