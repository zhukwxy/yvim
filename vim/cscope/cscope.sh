#!/bin/bash
basedir=`pwd`
find $basedir -regex '.*\.h\|.*\.c\|.*\.cpp\|.*\.hpp\|.*\.cxx\|.*\.hxx\|.*\.s\|.*\.S\|.*\.inl\|.*\.cc'>cscope.files
cscope -bkq -i $basedir/cscope.files                                                                                                                           
ctags -R --c++-kinds=+p+l+x+c+d+e+f+g+m+n+s+t+u+v --fields=+liaS --extra=+q --language-force=c++ $basedir
