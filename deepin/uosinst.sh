#!/bin/bash

curDir=`pwd`
user=`whoami`
if [ "$user" != "root" ] ; then
#下载git
#{{{
existGit=`which git`
if [ -z "$existGit" ] ; then
    sudo apt-get install git -y
fi
#}}}
#下载自动不全
sudo apt-get install bash-completion
#下载依赖
#{{{
sudo apt-get install python3 python3-dev python python-dev cmake build-essential -y
sudo apt-get install ctags cscope nodejs node-util libncurses-dev libx11-dev libxt-dev -y
#支持交叉编译
sudo apt-get install gcc-multilib g++-multilib -y
#}}}

#安装vim配置
#{{{
cp ./dpkg.vimrc ~/.vimrc
cp vim ~/.vim -rf
chmod 777 ~/.vim/cscope/*
#}}}

#安装YCM
#{{{
cd ~/.vim/plugged
cat YouCompleteMe.tar.gza* | tar -xvz
cd ~/.vim/plugged/YouCompleteMe
./install.py --clangd-completer
#}}}

#安装vim
#{{{
sudo apt autoremove vim -y
tar -xzvf $curDir/src/vim-8.2.0695.tar.gz -C $curDir/src
cd $curDir/src/vim-8.2.0695
./configure --with-features=huge \
    --enable-rubyinterp=yes \
    --enable-pythoninterp=yes \
    --enable-python3interp=yes \
    --enable-luainterp=yes \
    --enable-multibyte \
    --with-python-command=python2 \
    --with-python3-command=python3 \
    --enable-cscope \
    --enable-gtk2-check \
    --enable-gnome-check \
    --enable-gtk3-check \
    --enable-motif-check \
    --prefix=/usr \
    --with-x \
    --enable-largefile 
#    --with-python3-config-dir=/usr/lib/python3.8/config-3.8-x86_64-linux-gnu/
make CFLAGS="-O2 -D_FORTIFY_SOURCE=1" -j4
sudo make install
rm -rf $curDir/src/vim-8.2.0695
#}}}

#安装qtcreator
#{{{
#sudo apt-get install qt5-default qtcreator gdb -y
#sudo apt-get install manpages-dev manpages-posix manpages-posix-dev manpages 
#}}}

#安装cmake帮助手册
#{{{
#cd $curDir
#sudo cp ./resources/dpkg/CMake.qch /usr/share/qtcreator/doc
#assistant -register /usr/share/qtcreator/doc/CMake.qch
#cp ./resources/dpkg/org.qt-project.assistant.desktop ~/Desktop
#}}}
else
#安装vim配置
#{{{
cp ./dpkg-min.vimrc ~/.vimrc
cp vim ~/.vim -rf
chmod 777 ~/.vim/cscope/*
#}}}

#安装YCM
#{{{
cd ~/.vim/plugged
rm -rf YouCompleteMe.tar.gza*
#}}}

fi



