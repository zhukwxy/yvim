" Vim config file.
" Global Settings: {{{
"call pathogen#infect()              " use pathogen to manage plugins

call plug#begin('~/.vim/plugged')
"添加coc.nvim
Plug 'neoclide/coc.nvim', {'branch': 'release'}
"添加YCM
"Plug 'Valloric/YouCompleteMe'
"彩虹括号
Plug 'kien/rainbow_parentheses.vim'
"真彩色
Plug 'tpope/vim-sensible'



"添加本地插件
Plug '~/.vim/locplugged/vividchalk'
Plug '~/.vim/locplugged/solarized'
Plug '~/.vim/locplugged/autocomplpop/plugin'
Plug '~/.vim/locplugged/CmdlineComplete'
Plug '~/.vim/locplugged/gdb'
Plug '~/.vim/locplugged/nerdtree'
Plug '~/.vim/locplugged/nerdcommenter'
Plug '~/.vim/locplugged/mark'
Plug '~/.vim/locplugged/mru'
Plug '~/.vim/locplugged/echofunc'
Plug '~/.vim/locplugged/bufexplorer'
Plug '~/.vim/locplugged/taglist'
Plug '~/.vim/locplugged/snipMate'
Plug '~/.vim/locplugged/l9'
Plug '~/.vim/locplugged/jeffy-plugins'
Plug '~/.vim/locplugged/DfrankUtil'
Plug '~/.vim/locplugged/vimprj'
Plug '~/.vim/locplugged/indexer'
Plug '~/.vim/locplugged/vim-fswitch'
Plug '~/.vim/locplugged/pydiction'
"Plug '~/.vim/locplugged/c-vim'
Plug '~/.vim/locplugged/vim-airline/vim-airline'
Plug '~/.vim/locplugged/vim-airline/vim-airline-themes'
"Plug '~/.vim/locplugged/CompleteParameter'

call plug#end()

syntax on                           " highlight syntax
filetype plugin indent on           " auto detect file type
"auto Filetype python :call coc#config("suggest.autoTrigger", "always")
set nocompatible                    " out of Vi compatible mode
set number                          " show line number
set numberwidth=3                   " minimal culumns for line numbers
set textwidth=0                     " do not wrap words (insert)
set nowrap                          " do not wrap words (view)
set showcmd                         " show (partial) command in status line
set ruler                           " line and column number of the cursor position
set wildmenu                        " enhanced command completion
set wildmode=list:longest,full      " command completion mode
set laststatus=2                    " always show the status line
"set mouse=a                         " use mouse in all mode
set foldenable                      " fold lines
set foldmethod=marker               " fold as marker 
set noerrorbells                    " do not use error bell
set novisualbell                    " do not use visual bell
set t_vb=                           " do not use terminal bell
set smartindent                     "当在大括号中间回车的时候，他会智能缩进，因为他知道括号中间要缩进
"高亮现实当前行
set cursorline
"red（红），white（白），black（黑），green（绿），yellow（黄），blue（蓝），purple（紫），gray（灰），brown（棕），tan(褐色)，syan(青色)

set wildignore=.svn,.git,*.swp,*.bak,*~,*.o,*.a
set autowrite                       " auto save before commands like :next and :make
set hidden                          " enable multiple modified buffers
set history=100                     " record recent used command history
set autoread                        " auto read file that has been changed on disk
set backspace=indent,eol,start      " backspace can delete everything
set completeopt=menuone,longest     " complete options (insert)
set pumheight=10                    " complete popup height
set scrolloff=5                     " minimal number of screen lines to keep beyond the cursor
set autoindent                      " automatically indent new line
set cinoptions=:0,l1,g0,t0,(0,(s    " C kind language indent options

set tabstop=4                       " number of spaces in a tab
set softtabstop=4                   " insert and delete space of <tab>
set shiftwidth=4                    " number of spaces for indent
set expandtab                       " expand tabs into spaces
set incsearch                       " incremental search
set hlsearch                        " highlight search match
set ignorecase                      " do case insensitive matching
set smartcase                       " do not ignore if search pattern has CAPS
set nobackup                        " do not create backup file
set noswapfile                      " do not create swap file
set backupcopy=yes                  " overwrite the original file

set encoding=utf-8
set termencoding=utf-8
set fileencoding=utf-8
set fileencodings=gb2312,utf-8,gbk
set fileformat=unix
if $TERM == 'xterm-256color'
    set termguicolors "支持真彩色
endif
"set autochdir                           "自动切换当前目录为当前文件所在的目录和cscope跳转冲突
set  showmatch                          "插入括号时，短暂跳转到匹配的对应括号
"set magic                               "设置魔术
set fenc=
"灰色
set background=dark
"亮色
"set background=light
"colorscheme  darkblue

"配色方案 vividchalk
colorscheme vividchalk
"
"
"let g:solarized_termtrans=1

"if $COLORTERM == 'truecolor'
"    colorscheme solarizedgui
"else
"    set term=xterm
"    set t_Co=256
"    colorscheme solarized
"endif
"

" gui settings
if has("gui_running")
    set guioptions-=T " no toolbar
    set guioptions-=r " no right-hand scrollbar
    set guioptions-=R " no right-hand vertically scrollbar
    set guioptions-=l " no left-hand scrollbar
    set guioptions-=L " no left-hand vertically scrollbar
    autocmd GUIEnter * simalt ~x " window width and height
    source $VIMRUNTIME/delmenu.vim " the original menubar has an error on win32, so
    source $VIMRUNTIME/menu.vim    " use this menubar
    language messages zh_CN.utf-8 " use chinese messages if has
endif

" Restore the last quit position when open file.
autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \     exe "normal g'\"" |
    \ endif

"openGL着色器
au BufNewFile,BufRead *.frag,*.vert,*.fp,*.vp,*.glsl setf glsl
"}}}

" Key Bindings: {{{
let mapleader = ","
let maplocalleader = "\\"

" map : -> <space>
map <Space> :

" move between windows
nmap <C-h> <C-w>h
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-l> <C-w>l

" Don't use Ex mode, use Q for formatting
map Q gq

"make Y consistent with C and D
nnoremap Y y$

" toggle highlight trailing whitespace
nmap <silent> <leader>l :set nolist!<CR>

" Ctrol-E to switch between 2 last buffers
nmap <C-E> :b#<CR>

" ,e to fast finding files. just type beginning of a name and hit TAB
nmap <leader>e :e **/

" Make shift-insert work like in Xterm
map <S-Insert> <MiddleMouse>
map! <S-Insert> <MiddleMouse>

" ,n to get the next location (compilation errors, grep etc)
nmap <leader>n :cn<CR>
nmap <leader>p :cp<CR>

" Ctrl-N to disable search match highlight
nmap <silent> <C-N> :silent noh<CR>

" center display after searching
nnoremap n   nzz
nnoremap N   Nzz
nnoremap *   *zz
nnoremap #   #zz
nnoremap g*  g*zz
nnoremap g#  g#z

"*.cpp和*.h间的切换
nmap <silent> <leader>sw :FSHere<cr>

"YCM快捷键
nmap <silent> <leader>ge : YcmCompleter GoToDefinition<CR>

"buffer操作
"按Ctrl+h 向左移动一个buffer
"nnoremap <C-h> :bp<CR>
"按Ctrl+l 向右移动一个buffer
"nnoremap <C-l> :bn<CR>
"按Ctrl+^ 关闭当前buffer
"nnoremap <C-^> :bd<CR>

"}}}

" Plugin Settings: {{{
if has("win32") " win32 system
    let $HOME  = $VIM
    let $VIMFILES = $HOME . "/vimfiles"
else " unix
    let $HOME  = $HOME
    let $VIMFILES = $HOME . "/.vim"
endif

" mru 最近使用的文件
let MRU_Window_Height = 10
nmap <Leader>r :MRU<cr>

" taglist
let g:Tlist_WinWidth = 25
let g:Tlist_Use_Right_Window = 0
let g:Tlist_Auto_Update = 1
let g:Tlist_Process_File_Always = 1
let g:Tlist_Exit_OnlyWindow = 1
let g:Tlist_Show_One_File = 1
let g:Tlist_Enable_Fold_Column = 0
let g:Tlist_Auto_Highlight_Tag = 1
let g:Tlist_GainFocus_On_ToggleOpen = 1
nmap <Leader>t :TlistToggle<cr>
nmap <F4> :TlistToggle<cr>

" nerdtree
let g:NERDTreeWinPos = "right"
let g:NERDTreeWinSize = 30
let g:NERDTreeShowLineNumbers = 1
let g:NERDTreeQuitOnOpen = 1
nmap <Leader>f :NERDTreeToggle<CR>
nmap <Leader>F :NERDTreeFind<CR>

" snipMate
let g:snip_author   = "Jeffy Du"
let g:snip_mail     = "jeffy.du@163.com"
let g:snip_company  = "SIC Microelectronics CO. Ltd"

" man.vim - view man page in VIM
source $VIMRUNTIME/ftplugin/man.vim

" cscope
"find . -regex '.*\.h\|.*\.c\|.*\.cpp\|.*\.hpp\|.*\.cxx\|.*\.hxx\|.*\.s\|.*\.S'>cscope.files
"cscope -bkq -i cscope.files
"cs find s ---- 查找C语言符号，即查找函数名、宏、枚举值等出现的地方
"cs find g ---- 查找函数、宏、枚举等定义的位置，类似ctags所提供的功能
"cs find d ---- 查找本函数调用的函数
"cs find c ---- 查找调用本函数的函数
"cs find t ---- 查找指定的字符串
"cs find e ---- 查找egrep模式，相当于egrep功能，但查找速度快多了
"cs find f ---- 查找并打开文件，类似vim的find功能
"cs find i ---- 查找包含本文件的文
nmap <c-\>s :cs find s <C-R>=expand("<cword>")<cr><cr>
nmap <c-\>g :cs find g <C-R>=expand("<cword>")<cr><cr>
nmap <c-\>c :cs find c <C-R>=expand("<cword>")<cr><cr>
nmap <c-\>t :cs find t <C-R>=expand("<cword>")<cr><cr>
nmap <c-\>e :cs find e <C-R>=expand("<cword>")<cr><cr>
nmap <c-\>f :cs find f <C-R>=expand("<cfile>")<cr><cr>
nmap <c-\>i :cs find i <C-R>=expand("<cfile>")<cr><cr>
nmap <c-\>d :cs find d <C-R>=expand("<cword>")<cr><cr>



" vimgdb.vim
if has("gdb")
	set asm=0
	let g:vimgdb_debug_file=""
	run macros/gdb_mappings.vim
endif

if has("cscope")

    if filereadable("cscope.out")
        cs add cscope.out
    endif

endif

"YCM
"取消coc警告
let g:coc_disable_startup_warning = 1 

"if has("YCM")
set completeopt=menu,menuone
"关闭窗口预览
let g:ycm_add_preview_to_completeopt = 0
let g:ycm_max_num_candidates=50
let g:ycm_autoclose_preview_window_after_completion = 1
"关闭诊断信息
let g:ycm_show_diagnostics_ui=0
let g:ycm_enable_diagnostic_signs = 0
let g:ycm_enable_diagnostic_highlighting = 0

let g:ycm_server_log_level = 'debug'
let g:ycm_clangd_args = ['-log=verbose', '-pretty']
"2个字符就弹出提示
let g:ycm_min_num_of_chars_for_completion=2
let g:ycm_min_num_identifier_candidate_chars = 2
"是自动触发还是手动触发 0-手动触发 1-自动触发
let g:ycm_auto_trigger = 1
"配置路径
"let g:ycm_global_ycm_extra_conf='~/.vim/ycm/.ycm_extra_conf.py'
" 停止提示是否载入本地ycm_extra_conf文件
let g:ycm_confirm_extra_conf = 0
" 语法关键字补全
let g:ycm_seed_identifiers_with_syntax = 1
" 开启 YCM 基于标签引擎
let g:ycm_collect_identifiers_from_tags_files = 1
" 从第2个键入字符就开始罗列匹配项
let g:ycm_min_num_of_chars_for_completion=2
" 在注释输入中也能补全
let g:ycm_complete_in_comments = 1
" 在字符串输入中也能补全
let g:ycm_complete_in_strings = 1
" 注释和字符串中的文字也会被收入补全
let g:ycm_collect_identifiers_from_comments_and_strings = 1
" 弹出列表时选择第1项的快捷键(默认为<TAB>和<Down>)
let g:ycm_key_list_select_completion = ['<C-n>', '<Down>']
" 弹出列表时选择前1项的快捷键(默认为<S-TAB>和<UP>)
let g:ycm_key_list_previous_completion = ['<C-p>', '<Up>']
"开启clangd 1-开启 0-关闭
let g:ycm_use_clangd = 1

let g:ycm_use_ultisnips_completer=1
"禁止缓存匹配项，每次都重新生成匹配项
let g:ycm_cache_omnifunc=0
let g:ycm_disable_for_files_larger_than_kb = 1000
" 主动补全, 默认为<C-Space>
let g:ycm_key_invoke_completion = '<C-Space>'
" 停止显示补全列表(防止列表影响视野), 可以按<C-Space>重新弹出
let g:ycm_key_list_stop_completion = ['<CR>']
"增加对C语言系列的支持
"let g:ycm_clangd_binary_path = "/path/to/clangd"
"let g:ycm_semantic_triggers =  {
"  \   'c,h': ['->', '.'],
"  \   'cpp,hpp,cuda,objcpp': ['->', '.', '::'],
"  \ }


" 设置插件 indexer 调用 ctags 的参数
" 默认 --c++-kinds=+p+l，重新设置为 --c++-kinds=+p+l+x+c+d+e+f+g+m+n+s+t+u+v
" 默认 --fields=+iaS 不满足 YCM 要求，需改为 --fields=+iaSl
let g:indexer_ctagsCommandLineOptions="--c++-kinds=+p+l+x+c+d+e+f+g+m+n+s+t+u+v --fields=+iaSl --extra=+q"
"indexer 自己的配置文件，位于~/.indexer_files
"[foo] --配置文件的工程名
"/data/workspace/fool/src/--配置文件的源码目录
"endif
"python 插件补全 使用tab不全提示
let g:pydiction_location='~/.vim/locplugged/pydiction/complete-dict'
let g:pydiction_menu_height = 3
"
"
"airline配置

"let g:airline_theme='powerlineish'
"不检测空白
let g:airline#extensions#whitespace#enabled=0
"标题显示
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#left_sep = ' '
let g:airline#extensions#tabline#left_alt_sep = '|'
" 使用powerline打过补丁的字体
let g:airline_powerline_fonts = 1   

  if !exists('g:airline_symbols')
    let g:airline_symbols = {}
  endif

"unicode symbols
let g:airline_left_sep = '▶ '
let g:airline_left_alt_sep = '❯'
let g:airline_right_sep = '◀ '
let g:airline_right_alt_sep = '❮'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.crypt = '🔒'
let g:airline_symbols.linenr = '☰'
"let g:airline_symbols.linenr = '␊'
"let g:airline_symbols.linenr = '␤'
"let g:airline_symbols.linenr = '¶'
"let g:airline_symbols.maxlinenr = ''
let g:airline_symbols.maxlinenr = '㏑'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
"let g:airline_symbols.paste = 'Þ'
"let g:airline_symbols.paste = '∥'
let g:airline_symbols.spell = 'Ꞩ'
let g:airline_symbols.notexists = 'Ɇ'
let g:airline_symbols.whitespace = 'Ξ'




"}}}

"{{{
" vim 源码安装https://codeload.github.com/vim/vim/tar.gz/v8.2.1712
" ./configure --with-features=huge --enable-rubyinterp=yes 
" --enable-pythoninterp=yes --enable-python3interp=yes 
"  --enable-multibyte --with-python-config-dir=/usr/bin/python 
"  --with-python-config-dir=/usr/include/python3.7m --enable-cscope 
"  --prefix=/usr
"
"插件安装
"1. 安装C++
"   1) sudo apt-get install clang
"   2) ./install.py --clangd-completer
"   3) 如果要自动补全工程文件使用~/.vim/loycm/ycm.sh 脚本
"   3.1) 使用cmake生成compile_commands.json脚本,放在工程文件的顶层目录
"    3.1.1)cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON /或者
"           set(CMAKE_EXPORT_COMPILE_COMMANDS ON)
"    3.1.2) 拷贝compile_commands.json 到工程文件的根目录
"   4) YCM文档
"       https://github.com/ycm-core/YouCompleteMe#c-family-semantic-completion
"   5) llvm 下载
"       https://releases.llvm.org/download.html
"
"2. 安装cmake
"   1) sudo apt-get install cmake
"   2) 在vim下安装:CocInstall coc-cmake
"3. 安装coc-nvim 
"   1) 安装node.js
"   https://nodejs.org/en/download/ 官网
"   wget https://nodejs.org/dist/v12.18.3/node-v12.18.3.tar.gz
"   老的nodejs网站https://nodejs.org/zh-cn/download/releases/
"   2) curl --compressed -o- -L https://yarnpkg.com/install.sh | bash
"4 . 安装indexer插件
"    https://github.com/vim-scripts/vimprj
"    https://github.com/vim-scripts/DfrankUtil
"    https://github.com/vim-scripts/indexer.tar.gz
"5. ctags 
" ctags --list-languages --查看ctags支持的语言
" ctags -R --c++-kinds=+p+l+x+c+d+e+f+g+m+n+s+t+u+v --fields=+liaS --extra=+q --language-force=c++
" 生成的标签
" ctags --list-kinds=c++ --查看c++标签类型
"
"附加：
"配置的方法
"https://github.com/yangyangwithgnu/use_vim_as_ide
"分卷压缩和解压缩
"压缩
"tar -czvf - jdk1.8.0_221/ |split -b 98m - jdk1.8.0_221.tar.gz
"cat jdk1.8.0_221.tar.gza* | tar -xzv
"
"缓冲区操作
"1、列出缓冲区 :buffers, :files, :ls, 加入!强制列出所有缓冲区
"2、选择缓冲区 :buffer number , :buffer filename
"3、切换缓冲区 :bnext 到下一个缓冲区, :bprevious 前一个缓冲区
"4、删除缓冲区 : bdelete filename :bdelete number :number bdelete
":n1,n2 bdelete 删除指定缓冲区 加!强制卸载缓冲区
"5、卸载缓冲区 bunload{!}
"}}}





