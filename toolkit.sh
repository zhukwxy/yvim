#!/bin/bash
set -e
#=======================================================================================
#ARCH 
function arch() {
	echo $(uname -m)
}
#ARCH=$(arch)

#=======================================================================================
#kernel version
function kernid() {
	echo $(uname -r | cut -d '-' -f 1)
}

function kernid_major() {
	echo $(uname -r | cut -d '-' -f 1 | cut -d '.' -f 1)
}

function kernid_minor() {
	echo $(uname -r | cut -d '-' -f 1 | cut -d '.' -f 2)
}

#=======================================================================================
#file system id
function osid() {
	echo $(cat /etc/os-release | grep ^ID= | cut -d '=' -f 2)
}

#file system version code name
function oscn() {
	echo $(cat /etc/os-release | grep ^VERSION_CODENAME= | cut -d '=' -f 2)
}

function osvid() {
	local vid_=0
	vid_=$(cat /etc/os-release | grep ^VERSION_ID= | cut -d '=' -f 2)
	vid_=${vid_//\"/}
	echo ${vid_}
}

#=======================================================================================



